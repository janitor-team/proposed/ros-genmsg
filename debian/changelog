ros-genmsg (0.6.0-1) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Drop old Breaks/Replaces

  [ Timo Röhling ]
  * New upstream version 0.6.0
  * Add myself to uploaders
  * Bump Standards-Version to 4.6.1

 -- Timo Röhling <roehling@debian.org>  Thu, 13 Oct 2022 20:54:31 +0200

ros-genmsg (0.5.16-5) unstable; urgency=medium

  * Add special handling for Debian /usr workspace

 -- Timo Röhling <roehling@debian.org>  Tue, 21 Sep 2021 22:40:15 +0200

ros-genmsg (0.5.16-4) unstable; urgency=medium

  * Simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 11:35:59 +0100

ros-genmsg (0.5.16-3) unstable; urgency=medium

  * Drop arch path from d/rules

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 13:31:57 +0200

ros-genmsg (0.5.16-2) unstable; urgency=medium

  * Generalize d/rules

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 09:40:56 +0200

ros-genmsg (0.5.16-1) unstable; urgency=medium

  * New upstream version 0.5.16
  * Remove Thomas from Uploaders, thanks for working on this
  * bump policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 09:22:10 +0200

ros-genmsg (0.5.14-1) unstable; urgency=medium

  * New upstream version 0.5.14

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 18 Jan 2020 20:33:30 +0100

ros-genmsg (0.5.12-3) unstable; urgency=medium

  * Drop Python 2 package (Closes: #938377)
  * Bump policy versions (no changes)
  * simplify d/watch

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 26 Oct 2019 08:59:28 +0200

ros-genmsg (0.5.12-2) unstable; urgency=medium

  * python3-genmsg depends on python-genmsg for cmake files

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 25 Oct 2019 17:42:32 +0200

ros-genmsg (0.5.12-1) unstable; urgency=medium

  * New upstream version 0.5.12
  * rebase patches
  * Bump policy version (no changes)
  * switch to debhelper-compat and debhelper 12
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 30 Jul 2019 22:27:15 +0200

ros-genmsg (0.5.11-2) unstable; urgency=medium

  * Move pkgconfig to share

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 18 Jun 2018 19:04:33 +0200

ros-genmsg (0.5.11-1) unstable; urgency=medium

  * Update copyright terms
  * New upstream version 0.5.11
  * Fix Lintian warning

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 27 May 2018 12:22:31 +0200

ros-genmsg (0.5.10-2) unstable; urgency=medium

  * Bump policy version and remove get-orig-source
  * Add Python 3 package
  * Remove unused override_dh_auto_install in rules

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 09 Apr 2018 07:35:35 +0200

ros-genmsg (0.5.10-1) unstable; urgency=medium

  * Remove old catkin version
  * Update Vcs URLs to salsa.d.o
  * Move cleanup to d/clean
  * Add R³
  * http -> https
  * New upstream version 0.5.10
  * Update policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 31 Mar 2018 22:30:51 +0200

ros-genmsg (0.5.9-1) unstable; urgency=medium

  * Update watch file
  * New upstream version 0.5.9
  * Update policy and debhelper versions

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 02 Aug 2017 16:52:36 +0200

ros-genmsg (0.5.8-1) unstable; urgency=medium

  * Simplify rules
  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Update Vcs URLs
  * Update my email address
  * Remove lintian override, fixed in lintian 2.5.47
  * New upstream version 0.5.8

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 17 Sep 2016 18:07:16 +0200

ros-genmsg (0.5.7-1) unstable; urgency=medium

  * Imported Upstream version 0.5.7
  * Refresh patches

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 18 Jun 2016 11:06:44 +0200

ros-genmsg (0.5.6-5) unstable; urgency=medium

  * Add missing dependency

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Mon, 22 Feb 2016 15:34:10 +0100

ros-genmsg (0.5.6-4) unstable; urgency=medium

  * Adopt to new libexec location in catkin

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 16 Feb 2016 08:32:46 +0100

ros-genmsg (0.5.6-3) unstable; urgency=medium

  * Convert to new catkin with multiarch

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 24 Nov 2015 20:49:12 +0100

ros-genmsg (0.5.6-2) unstable; urgency=medium

  * install pkg-config file for rosmake

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Tue, 17 Nov 2015 10:17:35 +0100

ros-genmsg (0.5.6-1) unstable; urgency=medium

  * Initial release (Closes: #804005).

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sun, 15 Nov 2015 21:40:12 +0000
